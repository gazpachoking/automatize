import asyncio
import importlib
from pathlib import Path
import pkgutil

from jinja2.nativetypes import NativeEnvironment

from automatize.node import Node
from automatize.entry import Entry

for finder, name, ispkg in pkgutil.iter_modules(
    [Path(__file__).parent / "plugins"], "automatize.plugins."
):
    importlib.import_module(name)

nodes = {subclass.name: subclass() for subclass in Node.__subclasses__()}

pipeline = [
    ("debug_mock", {"entries": [{"path": ".."}]}),
    ("debug_print", {}),
    ("filesystem", {"path": "{{path}}", "glob": "*.csv"}),
    ("debug_print", {}),
    ("csv", {"path": "{{path}}"}),
    ("debug_print", {}),
    ("rss", {"url": "{{url}}"}),
    ("debug_print", {}),
    ("download", {"url": "{{link}}", "path": "./download"}),
    ("debug_print", {}),
]


def render_config(config, entry):
    environment = NativeEnvironment()
    rendered_config = {}
    for key, value in config.items():
        if isinstance(value, str):
            template = environment.from_string(value)
            value = template.render(entry)
        rendered_config[key] = value
    return rendered_config


async def run(pipeline, entry: Entry):
    loop = asyncio.get_event_loop()
    futures = []
    (node_name, config), *next_pipeline = pipeline
    config = render_config(config, entry)
    result = nodes[node_name].process(entry, config)
    entry_proceeded = False
    async for next_entry in result:
        if next_entry is entry:
            entry_proceeded = True
        else:
            entry.children.append(next_entry)
        if next_pipeline:
            futures.append(loop.create_task(run(next_pipeline, next_entry)))
        else:
            next_entry.finished.set_result(True)
    if not entry_proceeded:
        entry.finished.set_result(True)
    await asyncio.gather(*futures)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(pipeline, Entry()))
