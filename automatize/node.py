import abc
from typing import AsyncGenerator

from automatize.entry import Entry


class Node(abc.ABC):
    @property
    @abc.abstractmethod
    def name(self) -> str:
        """Name of the node"""

    @abc.abstractmethod
    async def process(self, entry: Entry, config: dict) -> AsyncGenerator[dict, None]:
        """This (async generator) method will be passed an entry, and it can yield 0 or more entries back out."""
