import asyncio
from typing import MutableMapping


class Entry(MutableMapping):
    def __init__(self, *args, **kwargs):
        self.store = dict(*args, **kwargs)
        self.children = []
        self.finished = asyncio.Future()

    def __setitem__(self, k, v) -> None:
        self.store[k] = v

    def __delitem__(self, k) -> None:
        del self.store[k]

    def __getitem__(self, k):
        return self.store[k]

    def __len__(self) -> int:
        return len(self.store)

    def __iter__(self):
        return iter(self.store)

    def __repr__(self):
        return repr(self.store)

    async def children_finished(self):
        await self.finished
        await asyncio.gather(*[e.children_finished() for e in self.children])
