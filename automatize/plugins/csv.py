import csv

from automatize.entry import Entry
from automatize.node import Node


class CSV(Node):
    name = "csv"

    async def process(self, entry: dict, config: dict):
        with open(config["path"], newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                yield Entry(row)
