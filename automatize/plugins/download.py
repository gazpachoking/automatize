from pathlib import Path
import re

import aiohttp

from automatize.node import Node


def guess_filename(response):
    if "content-disposition" in response.headers:
        filename = re.search("filename=(.+)", response.headers["content-disposition"])
        if filename:
            return filename
    return response.url.path.split("/")[-1]


class Download(Node):
    name = "download"

    async def process(self, entry: dict, config: dict):
        path = Path(config["path"])
        if not path.is_dir():
            path.mkdir(parents=True)
        async with aiohttp.ClientSession() as session:
            async with session.get(config["url"]) as response:
                if "filename" in config:
                    filename = config["filename"]
                else:
                    filename = guess_filename(response)
                filepath = path / filename
                with filepath.open("wb") as fd:
                    while True:
                        chunk = await response.content.read(10000)
                        if not chunk:
                            break
                        fd.write(chunk)
        entry["path"] = filepath
        yield entry
