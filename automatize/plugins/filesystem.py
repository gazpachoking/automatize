import pathlib

from automatize.entry import Entry
from automatize.node import Node


class Filesystem(Node):
    name = "filesystem"

    async def process(self, entry, config):
        path = pathlib.Path(config["path"])
        if "glob" in config:
            files = path.glob(config["glob"])
        else:
            files = path.iterdir()
        for file in files:
            yield Entry(path=file)
