from automatize.entry import Entry
from automatize.node import Node


class DebugMock(Node):
    name = "debug_mock"

    async def process(self, entry: Entry, config: dict):
        for e in config["entries"]:
            yield Entry(e)
