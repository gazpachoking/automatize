import aiohttp
import feedparser

from automatize.entry import Entry
from automatize.node import Node


class RSS(Node):
    name = "rss"

    config_schema = {
        "type": "object",
        "properties": {"url": {"type": "string"}, "format": "uri"},
    }

    async def process(self, entry, config):
        async with aiohttp.ClientSession() as session:
            async with session.get(config["url"]) as response:
                raw_feed = await response.text()
        feed = feedparser.parse(raw_feed)
        for result in feed.entries:
            yield Entry(result)
