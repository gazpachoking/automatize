from automatize.node import Node


class DebugPrint(Node):
    name = "debug_print"

    async def process(self, entry, config):
        print(entry)
        yield entry
